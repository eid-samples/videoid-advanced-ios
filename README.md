# iOS VideoID Demo

This is a demo application which shows how to use of the iOS SDK to record VideoIDs in Unattended** and Attended mode.

It shows how to:

* Start a basic controller.
* Create and start a custom controller.
* Use of custom messages.
* Change some UI styles.


## Requirements

The only requirement to run the application is to complete the environment configuration:
  - URL: The URL where VideoID service is running
  - Bearer: The authorization token to access the service

And set the idType of the document to be scanned (e.g. 62 for Spanish ID card).
