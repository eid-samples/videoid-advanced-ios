//
//  CustomViewController.swift
//  VideoTest
//
//  Created by chaparro on 06/11/2018.
//  Copyright © 2018 Electronic ID. All rights reserved.
//

import UIKit
import VideoID

import AlamofireImage

class AllNotificationsView: CustomMultimediaNotificationView{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    override func setValues(title: String, message: String, actionTitle: String, videoUrl: String, imageUrl: String, audioUrl: String, ttl: Int, phase: Phase?, inputType: CustomMultimediaNotificationViewInputType, viewType: CustomMultimediaNotificationViewType, finished: ((String?) -> ())?) {
        
        super.setValues(title: title, message: message, actionTitle: actionTitle, videoUrl: videoUrl, imageUrl: imageUrl, audioUrl: audioUrl, ttl: ttl, phase: phase, inputType: inputType, viewType: viewType, finished: finished)
        setupView()
        DispatchQueue.main.asyncAfter(deadline: .now() +  .seconds(5)) {
            self.isHidden = true
        }
        self.setAck(data: "")
    }
    
    func setupView() {
        self.backgroundColor = .red
        let label = UILabel()
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 20).isActive = true
        label.widthAnchor.constraint(equalToConstant: 200).isActive = true
        label.text = self.title
        
    }
    
}

class FeedBackNotificationsView: CustomMultimediaNotificationView{
    var button: UIButton?
    override func setValues(title: String, message: String, actionTitle: String, videoUrl: String, imageUrl: String, audioUrl: String, ttl: Int, phase: Phase?, inputType: CustomMultimediaNotificationViewInputType, viewType: CustomMultimediaNotificationViewType, finished: ((String?) -> ())?) {
        
        super.setValues(title: title, message: message, actionTitle: actionTitle, videoUrl: videoUrl, imageUrl: imageUrl, audioUrl: audioUrl, ttl: ttl, phase: phase, inputType: inputType, viewType: viewType, finished: finished)
        setupView()
        
        
    }
    
    func setupView() {
        self.backgroundColor = .blue
        self.button = UIButton()
        self.addSubview(button!)
        button?.translatesAutoresizingMaskIntoConstraints = false
        
        button?.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        button?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        button?.heightAnchor.constraint(equalToConstant: 20).isActive = true
        button?.widthAnchor.constraint(equalToConstant: 200).isActive = true
        button?.setTitle("Feed button title", for: .normal)
        button?.layer.borderColor = UIColor.red.cgColor
        button?.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        self.setAck(data: "")
        self.isHidden  = true
    }
}




class CaptchaNotificationsView: CustomMultimediaNotificationView ,UITextFieldDelegate{
    
    var textfield: UITextField?
    override func setValues(title: String, message: String, actionTitle: String, videoUrl: String, imageUrl: String, audioUrl: String, ttl: Int, phase: Phase?, inputType: CustomMultimediaNotificationViewInputType, viewType: CustomMultimediaNotificationViewType, finished: ((String?) -> ())?) {
        
        super.setValues(title: title, message: message, actionTitle: actionTitle, videoUrl: videoUrl, imageUrl: imageUrl, audioUrl: audioUrl, ttl: ttl, phase: phase, inputType: inputType, viewType: viewType, finished: finished)
        setupView()
        
    }
    
    func setupView() {
        self.backgroundColor = .red
        let imageView = UIImageView()
        
        self.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        
        textfield = UITextField()
        textfield!.delegate = self
        self.addSubview(textfield!)
        textfield?.translatesAutoresizingMaskIntoConstraints = false
        textfield?.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 10).isActive = true
        textfield?.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        textfield?.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        textfield?.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let button = UIButton()
        self.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.topAnchor.constraint(equalTo: textfield!.bottomAnchor, constant: 10).isActive = true
        button.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        button.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        button.setTitle("Captcha button", for: .normal)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        var urlRequest = URLRequest(url: URL(string: self.imageUrl!)!)
        urlRequest.setValue("*/*", forHTTPHeaderField: "Accept")
        imageView.af_setImage(withURLRequest: urlRequest)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        self.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        self.setAck(data: textField.text ?? "")
        return false
        
    }
}

class FrontNotificationsView: CustomMultimediaNotificationView{
    
    var label: UILabel?
    override func setValues(title: String, message: String, actionTitle: String, videoUrl: String, imageUrl: String, audioUrl: String, ttl: Int, phase: Phase?, inputType: CustomMultimediaNotificationViewInputType, viewType: CustomMultimediaNotificationViewType, finished: ((String?) -> ())?) {
        
        super.setValues(title: title, message: message, actionTitle: actionTitle, videoUrl: videoUrl, imageUrl: imageUrl, audioUrl: audioUrl, ttl: ttl, phase: phase, inputType: inputType, viewType: viewType, finished: finished)
        setupView()
        self.setAck(data: "")
        DispatchQueue.main.asyncAfter(deadline: .now() +  .seconds(5)) {
            self.isHidden = true
        }
    }
    
    func setupView() {
        self.backgroundColor = .cyan
        label = UILabel()
        self.addSubview(label!)
        label?.translatesAutoresizingMaskIntoConstraints = false
        
        label?.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        label?.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        label?.heightAnchor.constraint(equalToConstant: 20).isActive = true
        label?.widthAnchor.constraint(equalToConstant: 200).isActive = true
        label?.text = "Front title"
        
    }
}
class BackNotificationsView: FrontNotificationsView{
    
    override func setupView() {
        super.setupView()
        label?.text = "Back title"
        
    }
    
}

class FaceNotificationsView: FrontNotificationsView{
    
    override func setupView() {
        super.setupView()
        self.backgroundColor = .purple
        label?.text = "Face title"
    }
    
}

class HologramNotificationsView: FrontNotificationsView{
    
    override func setupView() {
        super.setupView()
        self.backgroundColor = .lightGray
        label?.text = "Hologram title"
    }
    
}

class AudioNotificationsView: FeedBackNotificationsView{
    
    override func setupView() {
        super.setupView()
        self.backgroundColor = .magenta
        button?.setTitle("Audio button title", for: .normal)
    }
    
}


class OnlyOneNotificationViewController: VideoIDViewController {
    
    
    override func getCustomMultimediaNotification() -> CustomMultimediaNotificationView? {
        let all = AllNotificationsView()
        return all
    }
}


class AllNotificationsViewController: VideoIDViewController {
    
    
    override func getCustomFeedbackMultimediaNotification() -> CustomMultimediaNotificationView? {
        let feedback = FeedBackNotificationsView()
        
        return feedback
    }
    
    override func getCustomFrontMultimediaNotification() -> CustomMultimediaNotificationView? {
        let front = FrontNotificationsView()
        
        return front
    }
    
    override func getCustomBackMultimediaNotification() -> CustomMultimediaNotificationView? {
        let back = BackNotificationsView()
        
        return back
    }
    
    override func getCustomFaceMultimediaNotification() -> CustomMultimediaNotificationView? {
        let face = FaceNotificationsView()
        
        return face
    }
    override func getCustomCaptchaMultimediaNotification() -> CustomMultimediaNotificationView? {
        let cap = CaptchaNotificationsView()
        return cap
    }
    
    override func getCustomHologramMultimediaNotification() -> CustomMultimediaNotificationView? {
        let hol = HologramNotificationsView()
        return hol
    }
    
    override func getCustomAudioCaptchaMultimediaNotification() -> CustomMultimediaNotificationView? {
        return  AudioNotificationsView()
        
    }
    
}


