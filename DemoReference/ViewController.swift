//
//  ViewController.swift
//  DemoReference
//
//  Copyright © 2017 Electronic Identification. All rights reserved.
//

import UIKit
import VideoID
import FaceID
import VideoScan
import Alamofire
import ObjectMapper

class ViewController: UIViewController {
    
    let endpoint = "https://etrust-sandbox.electronicid.eu/v2"
    let bearer = "your bearer"
    let rAuthority = "your rauthority"
        
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // override VideoID styles
        VideoID.Style.Roi.linesColorSuccess = UIColor.blue
        VideoID.Style.Tick.linesColor = UIColor.blue
        VideoID.Style.Notification.backgroundColor = .red
        VideoID.Style.MultimediaNotification.messageColor = .red
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startVideoID(_ sender: Any) {
        
        createAuthorization(service: .videoID, attended: false, onResult: { (auth) in
            let environment: VideoID.Environment = Environment(url: self.endpoint, autorization: auth!)
            let videoViewController = VideoIDViewController(environment: environment, language: "es", docType: 62)
            
            videoViewController.delegate = self
            videoViewController.modalPresentationStyle = .fullScreen
            self.show(videoViewController, sender: self)
            
        }) { (error) in
            self.onError(code: "Error", message: "No se pudo crear autorizacion")
        }
    }
    
    @IBAction func startVideoScan(_ sender: Any) {
        
        createAuthorization(service: .videoScan, attended: false, onResult: { (auth) in
            let environment: VideoScan.Environment = Environment(url: self.endpoint, autorization: auth!)
            let videoScanViewController = VideoScanViewController(environment: environment, language: "es", docType: 62)
            
            videoScanViewController.delegate = self
            videoScanViewController.modalPresentationStyle = .fullScreen
            self.show(videoScanViewController, sender: self)
            
        }) { (error) in
            self.onError(code: "Error", message: "No se pudo crear autorizacion")
        }
    }
    
    @IBAction func startConferenceID(_ sender: Any) {
        createAuthorization(service: .videoID, attended:true, onResult: { (auth) in
            let environment: VideoID.Environment = Environment(url: self.endpoint, autorization: auth!)
            
            let videoViewController = VideoIDViewController(environment: environment, language: "es", docType: 62)
            
            videoViewController.delegate = self
            videoViewController.modalPresentationStyle = .fullScreen
            self.show(videoViewController, sender: self)
            
        }) { (error) in
            self.onError(code: "Error", message: "No se pudo crear autorizacion")
        }
        
    }
    
    @IBAction func startSmileID(_ sender: Any) {
        
        createAuthorization(service: .smileID, attended: false, onResult: { (auth) in
            let environment: FaceID.Environment = Environment(url: self.endpoint, autorization: auth!)
            let faceIDViewController = FaceIDViewController(environment: environment, language: "es")
            
            faceIDViewController.delegate = self
            faceIDViewController.modalPresentationStyle = .fullScreen
            self.show(faceIDViewController, sender: self)
            
        }) { (error) in
            self.onError(code: "Error", message: "No se pudo crear autorizacion")
        }
    }
    
    @IBAction func allNotificationAction(_ sender: Any) {
        
        createAuthorization(service: .videoID, attended:false, onResult: { (auth) in
            let environment: VideoID.Environment = Environment(url: self.endpoint, autorization: auth!)
            let videoViewController = AllNotificationsViewController(environment: environment, language: "es", docType: 62)
            
            videoViewController.delegate = self
            videoViewController.modalPresentationStyle = .fullScreen
            self.show(videoViewController, sender: self)
            
        }) { (error) in
            self.onError(code: "Error", message: "No se pudo crear autorizacion")
        }
        
    }
    
    @IBAction func oneNotificationAction(_ sender: Any) {
        
        createAuthorization(service: .videoID, attended:false, onResult: { (auth) in
            let environment: VideoID.Environment = Environment(url: self.endpoint, autorization: auth!)
            let videoViewController = OnlyOneNotificationViewController(environment: environment, language: "es", docType: 62)
            
            videoViewController.delegate = self
            videoViewController.modalPresentationStyle = .fullScreen
            self.show(videoViewController, sender: self)
            
        }) { (error) in
            self.onError(code: "Error", message: "No se pudo crear autorizacion")
        }
    }
    
    private func createAuthorization(service: VideoID.VideoService, attended:Bool, onResult: @escaping (String?) -> Void, onError: @escaping (Error?) -> Void) {
        
        let headers:HTTPHeaders = ["Authorization": "Bearer " + self.bearer, "Accept": "application/json"]
        
        var serviceURL:String = ""
        
        switch service {
        
        case .videoID:
            serviceURL = "/videoid.request"
        case .videoScan:
            serviceURL = "/videoscan.request"
        case .smileID:
            serviceURL = "/smileid.request"
        case .undefined:
            break
        @unknown default:
            break
        }
        
        let request = AuthorizationRequest(tenantId: "",
                                           process: attended ? .attended : .unattended,
                                           rauthorityId: self.rAuthority)
        
        AF.request(endpoint + serviceURL,
                   method: .post,
                   parameters: request.toJSON(),
                   encoding: JSONEncoding.default,
                   headers: headers).validate().responseDecodable { (response: DataResponse<AuthorizationResponse, AFError>) in
                    
                    switch response.result {
                        
                    case .success:
                        do {
                            
                            try onResult(response.result.get().authorization)
                            
                        } catch {
                            
                            print("VideoIDType error: %@", error)
                            onError(error)
                        }
                        
                    case .failure(let error):
                        onError(error)
                    }
        }
    }
        
}

extension ViewController: VideoID.VideoDelegate, VideoScan.VideoDelegate, FaceID.VideoDelegate {
    
    func onComplete(videoID: String) {
        
        let alert = UIAlertController(title: "Success", message: "VideoID: \(videoID)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func onError(code: String, message: String?) {
        
        let alert = UIAlertController(title: "Error", message: "\(code): \(message ?? "")", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}



class AuthorizationRequest: Mappable {
    
    var tenantId: String?
    var process: VideoID.Process?
    var rauthorityId: String?
    
    init(tenantId: String?, process: VideoID.Process?, rauthorityId: String) {
        self.tenantId = tenantId
        self.process = process
        self.rauthorityId = rauthorityId
    }
    
    required init?(map: Map) {}
    
    public func mapping(map: Map) {
        tenantId <- map["tenantId"]
        process <- map["process"]
        rauthorityId <- map["rauthorityId"]
    }
}

class AuthorizationResponse: Decodable {
    
    var id: String?
    var authorization: String?
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case authorization
        
    }
    
}


